import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";

//egenskap handlekurv, scenario 1
Given(/^at jeg har åpnet nettkiosken$/, () => {
    cy.visit('http://localhost:8080');
});

When(/^jeg legger inn varer og kvanta$/, () => {
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('4');
    cy.get('#saveItem').click();
});

Then(/^skal handlekurven inneholde det jeg har lagt inn$/, () => {
    cy.get('#cart').should('contain', "Hubba bubba")
});

And(/^den skal ha riktig totalpris$/, function () {
        cy.get('#price').should('have.text', '8');
});

//egenskap handlekurv, scenario 2
Given(/^at jeg har åpnet nettkiosken$/, () => {
    cy.visit('http://localhost:8080');
});

And(/^lagt inn varer og kvanta$/, () => {
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('4');
    cy.get('#saveItem').click();
    cy.get('#product').select('Hobby');
    cy.get('#quantity').clear().type('2');
    cy.get('#saveItem').click();
})

When(/^jeg sletter varer$/, () => {
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('4');
    cy.get('#deleteItem').click();
    cy.get('#product').select('Hobby');
    cy.get('#quantity').clear().type('3');
    cy.get('#saveItem').click();
});

Then(/^skal ikke handlekurven inneholde det jeg har slettet$/, () => {
    cy.get('#cart').should('not.contain', "Hubba bubba")
    cy.get('#cart').should('contain', '3 Hobby')
});



//egenskap handlekurv, scenario 3
Given(/^at jeg har åpnet nettkiosken$/, () => {
    cy.visit('http://localhost:8080');
});

And(/^lagt inn varer og kvanta$/, () => {
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('4');
    cy.get('#saveItem').click();
    cy.get('#product').select('Hobby');
    cy.get('#quantity').clear().type('2');
    cy.get('#saveItem').click();
})

When(/^jeg oppdaterer kvanta for en vare$/, () => {
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('2');
    cy.get('#saveItem').click();
    cy.get('#product').select('Hobby');
    cy.get('#quantity').clear().type('5');
    cy.get('#saveItem').click();

});

Then(/^skal handlekurven inneholde riktig kvanta for varen$/, () => {
   
    cy.get('#cart').should('contain', '2 Hubba bubba')
    cy.get('#cart').should('contain', '5 Hobby')

});


// egenskap betale, scenario 1
Given(/^at jeg har åpnet nettkiosken $/, () => {
    cy.visit('http://localhost:8080');
});

And(/^at jeg har lagt inn varer i handlekurven$/, () => {
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('4');
    cy.get('#saveItem').click();
    cy.get('#product').select('Hobby');
    cy.get('#quantity').clear().type('2');
    cy.get('#saveItem').click();
});

And(/^trykket på Gå til betaling$/, () => {
    cy.get('#goToPayment').click();
});

When(/^jeg legger inn navn, adresse, postnummer, poststed og kortnummer$/, () => {
    cy.get("#fullName").type("line")
    cy.get("#address").type("beverly hills 5")
    cy.get("#postCode").type("4661")
    cy.get("#city").type("holmenkollen")
    cy.get("#creditCardNo").type("1235553857356793")
});

And(/^trykker på Fullfør kjøp$/, () => {
    cy.get('form').submit();
});

Then(/^skal jeg få beskjed om at kjøpet er registrert$/, () => {
    cy.contains("Din ordre er registrert") 
});

// egenskap betale, scenario 2
Given(/^at jeg har åpnet nettkiosken $/, () => {
    cy.visit('http://localhost:8080');
});

And(/^at jeg har lagt inn varer i handlekurven$/, () => {
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('4');
    cy.get('#saveItem').click();
    cy.get('#product').select('Hobby');
    cy.get('#quantity').clear().type('2');
    cy.get('#saveItem').click();
});

And(/^trykket på Gå til betaling$/, () => {
    cy.get('#goToPayment').click();
});

When(/^jeg legger inn ugyldige verdier i feltene$/, () => {
    cy.get("#fullName").type("")
    cy.get("#address").type("beverly hills 5")
    cy.get("#postCode").type("4661")
    cy.get("#city").type("holmenkollen")
    cy.get("#creditCardNo").type("1235553857356793")
});

And(/^trykker på Fullfør kjøp$/, () => {
    cy.get('form').submit();
});

Then(/^skal jeg få feilmeldinger for disse$/, () => {
    cy.contains("Feltet må ha en verdi") 
});
